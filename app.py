#import library
from flask import Flask, render_template,request,jsonify,make_response
from flask_cors import CORS
import json
import mysql.connector
app = Flask(__name__)
app.config['JSON_AS_ASCII']=False
#To cross domain
CORS(app)
host ="localhost"
user ="root"
password =""
db="units_conversion"
port ="3307"
#API maintain units
@app.route("/api/listunit")
def read():
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    mycursor.execute("select * from units")
    myresult = mycursor.fetchall()
    print(myresult)
    return make_response(jsonify(myresult),200)

@app.route("/api/listunit/<id>")
def readbyid(id):
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    sql ="select * from units where id =%s"
    val=(id,)
    mycursor.execute(sql,val)
    myresult = mycursor.fetchall()
    return make_response(jsonify(myresult),200)

@app.route("/api/unit/",methods =['POST'])
def createunits():
    data = request.get_json()
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    sql ="insert into units (name,type) values(%s,%s)"
    val=(data['name'],data['type'])
    mycursor.execute(sql,val)
    mydb.commit()
    return make_response(jsonify({"rowcount ": mycursor.rowcount}),200)

@app.route("/api/unit/<id>",methods =['PUT'])
def updateunits(id):
    data = request.get_json()
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    sql ="update units set name = %s ,type = %s where id = %s"
    val=(data['name'],data['type'],id)
    mycursor.execute(sql,val)
    mydb.commit()
    return make_response(jsonify({"rowcount ": mycursor.rowcount}),200)

@app.route("/api/unit/<id>",methods =['DELETE'])
def deleteunits(id):
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    sql ="delete from units where id = %s"
    val=(id,)
    mycursor.execute(sql,val)
    mydb.commit()
    return make_response(jsonify({"rowcount ": mycursor.rowcount}),200)

# API maintain conversion_formula
@app.route("/api/conversionunitlist")
def readconversionunit():
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    mycursor.execute("select * from conversion_formula")
    myresult = mycursor.fetchall()
    print(myresult)
    return make_response(jsonify(myresult),200)

@app.route("/api/conversionunitlist/<id>")
def readconversionunitbyid(id):
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    sql ="select * from conversion_formula where id =%s"
    val=(id,)
    mycursor.execute(sql,val)
    myresult = mycursor.fetchall()
    return make_response(jsonify(myresult),200)

@app.route("/api/conversion/",methods =['POST'])
def createconversionunit():
    data = request.get_json()
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    sql ="insert into conversion_formula (unit_id,conversion_type,per_units) values(%s,%s)"
    val=(data['unit_id'],data['conversion_type'],data['per_units'])
    mycursor.execute(sql,val)
    mydb.commit()
    return make_response(jsonify({"rowcount ": mycursor.rowcount}),200)

@app.route("/api/conversion/<id>",methods =['PUT'])
def updateconversionunit(id):
    data = request.get_json()
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    sql ="update conversion_formula set conversion_type = %s ,per_units = %s where id = %s"
    val=(data['conversion_type'],data['per_units'],id)
    mycursor.execute(sql,val)
    mydb.commit()
    return make_response(jsonify({"rowcount ": mycursor.rowcount}),200)

@app.route("/api/conversion/<id>",methods =['DELETE'])
def deleteconversionunit(id):
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor=mydb.cursor(dictionary=True)
    sql ="delete from conversion_formula where id = %s"
    val=(id,)
    mycursor.execute(sql,val)
    mydb.commit()
    return make_response(jsonify({"rowcount ": mycursor.rowcount}),200)


@app.route("/api/conversionunit/<id>/<number>")
def conversionunit(id,number):
    mydb=mysql.connector.connect(host=host,user=user,password=password,db=db,port=port)
    mycursor = mydb.cursor()
    #mycursor=mydb.cursor(dictionary=True)
    sql ="select a.type,b.* from units a left join conversion_formula b on a.id=b.unit_id where a.id =%s"
    val=(id,)
    mycursor.execute(sql,val)
    myresult = mycursor.fetchall()  
    
    print ("Total rows are:  ", len(myresult)  ,"\n")   
    for row in myresult:
        try:                    
            print ("unit_id: ", row[2])
            print ("conversion from: ", row[0])
            print ("Conversion to: ", row[3])
            print ("Number of conversion: ",number)
            print ("Result: ", row[4]*float(number),row[3])
            print ("\n")
        except ValueError:
            print(row, "caused an error")
    mycursor.close()
    return render_template('index.html',data=myresult,number=float(number))
    